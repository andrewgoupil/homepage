var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
var DAYS = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
var WEATHER_API = "http://api.openweathermap.org/data/2.5/weather?q=Toronto_ca&units=metric&appid=3008b874c703ccace378d09dca34e3ab";
var BOOKMARKS = {
  "Social": [
    ["Facebook", "https://www.facebook.com/"],
    ["Messenger", "https://www.messenger.com"],
    ["Reddit", "https://www.reddit.com/"],
    ["Linkedin", "https://linkedin.com"],
    ["Gmail", "https://www.gmail.com"]
  ],

  "Repositories" : [
    ["Github", "https://www.github.com"],
    ["Bitbucket", "https://bitbucket.org/"],
    ["Share LaTeX", "https://www.sharelatex.com/"],
    ["Google Drive", "https://drive.google.com/"],
    ["AWS", "https://us-west-2.console.aws.amazon.com/console/home?region=us-west-2"]
  ],

  "U of T" : [
    ["ACORN", "http://www.acorn.utoronto.ca"],
    ["Portal", "https://portal.utoronto.ca"],
    ["CDF", "http://www.cdf.toronto.edu/"],
    ["UTMail+", "https://mail.utoronto.ca"],
    ["PEY", "http://www.apsc.utoronto.ca/portal/"]
  ],

  "Courses" : [
    ["CSC301", "https://csc301-fall-2016.github.io/"],
    ["CSC318", "https://portal.utoronto.ca"],
    ["CSC336", "http://www.cs.toronto.edu/~krj/courses/336/"],
    ["CSC343", "http://www.cdf.toronto.edu/~csc343h/fall/"],
    ["CSC373", "https://piazza-resources.s3.amazonaws.com/it0gghqjrz527k/it2fpey1uwz6tc/373_syllabus.htm?AWSAccessKeyId=AKIAIEDNRLJ4AZKBW6HA&Expires=1474426743&Signature=oGqlejjYvytt94lyUL0h8iMsMyA%3D"]
  ],

  "Entertainment" : [
    ["YouTube", "http://www.youtube.com"],
    ["Movies", "http://www.primewire.ag"],
    ["Torrents", "https://thepiratebay.org/"]
  ]
};

function getCurrentDate() {
  var dateObject = new Date();
  return DAYS[dateObject.getDay()] + ", " + MONTHS[dateObject.getMonth()] + " " + dateObject.getDate();
}

function makeIterable(x) {
  var arr = [];
  for(var i = 0; i < x; i++) {
    arr.push(i);
  }
  return arr;
}

var app = angular.module('HomepageLoader', []);
app.controller('MainController', ['$scope', '$http', function($scope, $http) {
  // Retrieving Weather Data
  $http.get(WEATHER_API)
    .then(function(response) {
        var weatherData = response.data;
        $scope.weather = Math.round(parseFloat(weatherData["main"]["temp"])) + "° & " + weatherData["weather"][0]["main"];
    });

  // Generating & Formatting current date
  $scope.date = getCurrentDate();

  // Generating Table
  var maxKey = "";
  var maxKeyLen = 0;
  for(var k in BOOKMARKS) {
    if(BOOKMARKS[k].length > maxKeyLen) {
      maxKey = k;
      maxKeyLen = BOOKMARKS[k].length;
    }
  }

  $scope.keys = Object.keys(BOOKMARKS);
  $scope.bookmarks = BOOKMARKS;
  $scope.maxRow = makeIterable(BOOKMARKS[maxKey].length);
}]);
